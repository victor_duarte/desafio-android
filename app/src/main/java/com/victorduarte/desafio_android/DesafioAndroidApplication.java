package com.victorduarte.desafio_android;

import android.app.Application;
import android.content.Context;

import com.victorduarte.desafio_android.interactors.ApiInteractor;

import io.realm.Realm;

/**
 * Created by Victor on 25/08/2016.
 */
public class DesafioAndroidApplication extends Application {

    private ApiInteractor apiInteractor;

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);

        apiInteractor = ApiInteractor.getApiInteractor(BuildConfig.URLBASE);
    }

    public static DesafioAndroidApplication get(Context context) {
        return (DesafioAndroidApplication) context.getApplicationContext();
    }

    public ApiInteractor getApiInteractor() {
        return apiInteractor;
    }

}
