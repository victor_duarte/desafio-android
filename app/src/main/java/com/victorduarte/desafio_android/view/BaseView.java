package com.victorduarte.desafio_android.view;

/**
 * Created by Victor on 25/08/2016.
 */
public interface BaseView {

    void showProgress();

    void hideProgress();

    void showError();

    void hideError();

    void cleanData();
}
