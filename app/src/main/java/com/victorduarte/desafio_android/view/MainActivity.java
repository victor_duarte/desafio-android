package com.victorduarte.desafio_android.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.victorduarte.desafio_android.R;

public class MainActivity extends AppCompatActivity {

    FrameLayout mainContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainContainer = (FrameLayout) findViewById(R.id.maincontainer);

        if (savedInstanceState != null) {
            return;
        }

        startFragment(new RepositoryListFragment());

        if (mainContainer == null) {
            showDetail(new RepositoryDetailFragment());
        }
    }

    public void startFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(mainContainer != null ? R.id.maincontainer : R.id.repository_list_container, fragment, null)
                .commit();
    }

    public void showDetail(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(mainContainer != null ? R.id.maincontainer : R.id.repository_detail_container,
                        fragment)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }
}
