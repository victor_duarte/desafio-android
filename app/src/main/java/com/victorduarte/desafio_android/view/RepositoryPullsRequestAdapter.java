package com.victorduarte.desafio_android.view;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.victorduarte.desafio_android.R;
import com.victorduarte.desafio_android.models.PullRequest;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Victor on 25/08/2016.
 */
public class RepositoryPullsRequestAdapter
        extends RecyclerView.Adapter<RepositoryPullsRequestAdapter.RepositoryPullsRequestViewHolder> {

    private List<PullRequest> items;
    private Context context;

    public RepositoryPullsRequestAdapter(Context context, List<PullRequest> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public RepositoryPullsRequestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pulls_request_row, parent, false);

        RepositoryPullsRequestViewHolder vh = new RepositoryPullsRequestViewHolder(v);
        return vh;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onBindViewHolder(RepositoryPullsRequestViewHolder holder, int position) {

        PullRequest item = items.get(position);

        holder.txtName.setText(item.getUser().getName());
        holder.txtUserName.setText(item.getUser().getLogin());
        holder.txtTitle.setText(item.getTitle());
        holder.txtBody.setText(item.getBody());

        holder.txtDate.setText(new android.text.format.DateFormat().format("dd/MM/yyyy", item.getCreatedAt()));

        Picasso.with(context)
                .load(item.getUser().getAvatarUrl())
                .placeholder(context.getResources().getDrawable(R.drawable.avatar))
                .into(holder.imgProfile);

        holder.layoutParent.setOnClickListener(view ->
                context.startActivity(
                new Intent(Intent.ACTION_VIEW,
                        Uri.parse(item.getHtmlUrl()))));
    }

    public static class RepositoryPullsRequestViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_user_name)
        public TextView txtUserName;

        @BindView(R.id.txt_name)
        public TextView txtName;

        @BindView(R.id.img_profile)
        public CircleImageView imgProfile;

        @BindView(R.id.txt_title)
        public TextView txtTitle;

        @BindView(R.id.txt_body)
        public TextView txtBody;

        @BindView(R.id.txt_date)
        public TextView txtDate;

        @BindView(R.id.layout_parent)
        public RelativeLayout layoutParent;

        public RepositoryPullsRequestViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
