package com.victorduarte.desafio_android.view;

import com.victorduarte.desafio_android.models.PullRequest;

import java.util.List;

/**
 * Created by Victor on 25/08/2016.
 */
public interface RepositoryDetailView extends BaseView{
    void showItems(List<PullRequest> items);
}
