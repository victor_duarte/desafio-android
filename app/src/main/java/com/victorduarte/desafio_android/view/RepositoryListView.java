package com.victorduarte.desafio_android.view;

import com.victorduarte.desafio_android.models.Repository;

import java.util.List;

/**
 * Created by Victor on 25/08/2016.
 */
public interface RepositoryListView extends BaseView {
    void showItems(List<Repository> items);
}
