package com.victorduarte.desafio_android.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.victorduarte.desafio_android.DesafioAndroidApplication;
import com.victorduarte.desafio_android.R;
import com.victorduarte.desafio_android.models.Repository;
import com.victorduarte.desafio_android.presenters.RepositoryListPresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class RepositoryListFragment extends Fragment implements RepositoryListView {
    @BindView(R.id.repository_list_recyclerview)
    RecyclerView repositoryListRecicler;

    @BindView(R.id.progress)
    ProgressBar progressBar;

    protected List<Repository> repositoryList;
    private RepositoryListAdapter repositoryListAdapter;
    Snackbar snackbar;

    public RepositoryListPresenter presenter;

    public RepositoryListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        presenter = new RepositoryListPresenter(this,
                DesafioAndroidApplication.get(getContext()).getApiInteractor());
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(R.string.respository_list_title);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_repository_list, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initRecyclerView();

        if (repositoryList == null || repositoryList.size() == 0) {
            presenter.getRepositories(getContext());
        } else {
            repositoryListAdapter = new RepositoryListAdapter(getActivity(), repositoryList);
            repositoryListRecicler.setAdapter(repositoryListAdapter);
        }
    }

    private void initRecyclerView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());

        repositoryListRecicler.setHasFixedSize(true);
        repositoryListRecicler.setLayoutManager(layoutManager);

        repositoryListRecicler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (((LinearLayoutManager) layoutManager)
                        .findLastCompletelyVisibleItemPosition() == layoutManager.getItemCount() - 1) {
                    presenter.getRepositories(getContext());
                }
            }
        });
    }

    @Override
    public void showItems(List<Repository> items) {
        if (repositoryListAdapter == null) {
            repositoryList = items;
            repositoryListAdapter = new RepositoryListAdapter(getActivity(), items);
            repositoryListRecicler.setAdapter(repositoryListAdapter);
        } else {
            repositoryList.addAll(items);
            repositoryListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void cleanData() {
        if (repositoryList != null) {
            repositoryList.clear();
        }
    }

    @Override
    public void showError() {
        snackbar = Snackbar
                .make(getView(),
                        R.string.repository_list_get_error,
                        Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.retry,
                        view -> presenter.getRepositories(getContext()));

        snackbar.show();
    }

    @Override
    public void hideError() {
        if (snackbar != null && snackbar.isShown()) {
            snackbar.dismiss();
        }
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }
}
