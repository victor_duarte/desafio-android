package com.victorduarte.desafio_android.view;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.victorduarte.desafio_android.R;
import com.victorduarte.desafio_android.models.Repository;
import com.victorduarte.desafio_android.utils.IntUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Victor on 25/08/2016.
 */
public class RepositoryListAdapter extends RecyclerView.Adapter<RepositoryListAdapter.RepositoryListViewHolder> {

    private List<Repository> items;
    private Activity activity;

    public RepositoryListAdapter(Activity activity, List<Repository> items) {
        this.activity = activity;
        this.items = items;
    }

    public List<Repository> getItems() {
        return items;
    }

    public Repository getItem(int position) {
        return items.get(position);
    }

    @Override
    public RepositoryListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.repository_list_row, parent, false);

        RepositoryListViewHolder vh = new RepositoryListViewHolder(v);

        return vh;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onBindViewHolder(RepositoryListViewHolder holder, int position) {
        final Repository item = items.get(position);

        holder.txtName.setText(item.getOwner().getName());
        holder.txtUserName.setText(item.getOwner().getLogin());
        holder.txtTitle.setText(item.getName());
        holder.txtDescription.setText(item.getDescription());
        holder.txtForkCount.setText(IntUtils.formatCount(item.getForksCount()));
        holder.txtStarCount.setText(IntUtils.formatCount(item.getStargazersCount()));

        Picasso.with(activity)
                .load(item.getOwner().getAvatarUrl())
                .placeholder(activity.getResources().getDrawable(R.drawable.avatar))
                .into(holder.imgProfile);

        holder.layoutParent.setOnClickListener(view ->
                ((MainActivity) activity).showDetail(
                        RepositoryDetailFragment.newInstance(
                                item.getOwner().getLogin(),
                                item.getName())));
    }

    public static class RepositoryListViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_user_name)
        public TextView txtUserName;

        @BindView(R.id.txt_name)
        public TextView txtName;

        @BindView(R.id.img_profile)
        public CircleImageView imgProfile;

        @BindView(R.id.txt_title)
        public TextView txtTitle;

        @BindView(R.id.txt_description)
        public TextView txtDescription;

        @BindView(R.id.txt_fork_count)
        public TextView txtForkCount;

        @BindView(R.id.txt_star_count)
        public TextView txtStarCount;

        @BindView(R.id.layout_parent)
        public RelativeLayout layoutParent;

        public RepositoryListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
