package com.victorduarte.desafio_android.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.victorduarte.desafio_android.DesafioAndroidApplication;
import com.victorduarte.desafio_android.R;
import com.victorduarte.desafio_android.models.PullRequest;
import com.victorduarte.desafio_android.presenters.RepositoryDetailPresenter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class RepositoryDetailFragment extends Fragment implements RepositoryDetailView {
    private static final String ARG_CREATOR = "creator";
    private static final String ARG_REPOSITORY = "repository";

    String creator, repository;

    public RepositoryDetailPresenter presenter;

    @BindView(R.id.repository_pulls_request_recyclerview)
    RecyclerView repositoryPullsRequestRecicler;

    @BindView(R.id.progress)
    ProgressBar progressBar;

    @BindView(R.id.txt_feedback)
    TextView txtFeedBack;

    private ArrayList<PullRequest> pullRequestList;
    private RepositoryPullsRequestAdapter repositoryPullsRequestAdapter;
    Snackbar snackbar;

    public RepositoryDetailFragment() {
        // Required empty public constructor
    }

    public static RepositoryDetailFragment newInstance(String creator, String repository) {
        RepositoryDetailFragment fragment = new RepositoryDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CREATOR, creator);
        args.putString(ARG_REPOSITORY, repository);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        presenter = new RepositoryDetailPresenter(this,
                DesafioAndroidApplication.get(getContext()).getApiInteractor());

        if (getArguments() != null) {
            creator = getArguments().getString(ARG_CREATOR);
            repository = getArguments().getString(ARG_REPOSITORY);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(repository);
    }

    @Override
    public void onPause() {
        super.onPause();

        hideError();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_repository_detail, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (creator == null && repository == null) {
            return;
        }

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());

        repositoryPullsRequestRecicler.setHasFixedSize(true);
        repositoryPullsRequestRecicler.setLayoutManager(layoutManager);

        if (pullRequestList == null || pullRequestList.size() == 0) {
            presenter.getPullRequest(getContext(), creator, repository);
        } else {
            repositoryPullsRequestAdapter = new RepositoryPullsRequestAdapter(getActivity(), pullRequestList);
            repositoryPullsRequestRecicler.setAdapter(repositoryPullsRequestAdapter);
        }
    }

    @Override
    public void showItems(List<PullRequest> items) {
        pullRequestList = (ArrayList<PullRequest>) items;
        repositoryPullsRequestAdapter = new RepositoryPullsRequestAdapter(getContext(), items);
        repositoryPullsRequestRecicler.setAdapter(repositoryPullsRequestAdapter);

        if (items.size() == 0) {
            txtFeedBack.setText(R.string.repository_detail_no_pull_request);
            txtFeedBack.setVisibility(View.VISIBLE);
        } else {
            txtFeedBack.setVisibility(View.GONE);
        }
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showError() {
        snackbar = Snackbar
                .make(getView(),
                        R.string.repository_detail_error,
                        Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.retry,
                        view -> presenter.getPullRequest(getContext(), creator, repository));

        snackbar.show();
    }

    @Override
    public void hideError() {
        if (snackbar != null && snackbar.isShown()) {
            snackbar.dismiss();
        }
    }

    @Override
    public void cleanData() {
        if (pullRequestList != null) {
            pullRequestList.clear();
        }
    }
}
