package com.victorduarte.desafio_android.interactors;

import com.victorduarte.desafio_android.ServiceContract;
import com.victorduarte.desafio_android.models.PullRequest;
import com.victorduarte.desafio_android.models.RepositoriesSearchResult;

import java.util.List;

import io.realm.Realm;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

/**
 * Created by Victor on 25/08/2016.
 */
public class ApiInteractor {

    ServiceContract serviceContract;

    public ApiInteractor(ServiceContract serviceContract) {
        this.serviceContract = serviceContract;
    }

    public Observable<RepositoriesSearchResult> searchRepositories(String filter, String sort, int pagination) {
        return serviceContract
                .searchRepository(filter, sort, pagination)
                .map(repositoriesSearchResult -> {
                    repositoriesSearchResult.setPagination(pagination);
                    Realm.getDefaultInstance().executeTransaction(realm ->
                            realm.copyToRealmOrUpdate(repositoriesSearchResult));
                    return repositoriesSearchResult;
                });
    }

    public Observable<RepositoriesSearchResult> getOfflineRepositories(int pagination) {
        Realm realm = Realm.getDefaultInstance();
        RepositoriesSearchResult result = realm.where(RepositoriesSearchResult.class)
                .equalTo("pagination", pagination).findFirst();

        return Observable.just(result != null ? realm.copyFromRealm(result) : null);
    }

    public Observable<List<PullRequest>> getOffilinePullRequests(String creator, String repository) {
        Realm realm = Realm.getDefaultInstance();
        List<PullRequest> result = realm.where(PullRequest.class)
                .equalTo("creator", creator)
                .equalTo("repository", repository).findAll();

        return Observable.just(result != null ? realm.copyFromRealm(result) : null);
    }

    public Observable<List<PullRequest>> getAllPullRequests(String creator, String repository) {
        return serviceContract.getAllPullRequests(creator, repository)
                .map(pullRequests -> {
                    for (PullRequest pullRequest : pullRequests) {
                        pullRequest.setCreator(creator);
                        pullRequest.setRepository(repository);
                    }
                    Realm.getDefaultInstance().executeTransaction(realm ->
                            realm.copyToRealmOrUpdate(pullRequests));
                    return pullRequests;
                });
    }

    public static ApiInteractor getApiInteractor(String url){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return new ApiInteractor(retrofit.create(ServiceContract.class));
    }

}
