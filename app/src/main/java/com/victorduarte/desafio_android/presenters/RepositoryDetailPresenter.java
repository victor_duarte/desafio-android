package com.victorduarte.desafio_android.presenters;

import android.content.Context;

import com.victorduarte.desafio_android.interactors.ApiInteractor;
import com.victorduarte.desafio_android.utils.NetworkUtils;
import com.victorduarte.desafio_android.view.RepositoryDetailView;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Victor on 25/08/2016.
 */
public class RepositoryDetailPresenter {
    private RepositoryDetailView view;
    private ApiInteractor interactor;

    private boolean firstTime = true;

    public RepositoryDetailPresenter(RepositoryDetailView view, ApiInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    public void getPullRequest(Context context, String creator, String repository) {
        view.showProgress();

        if (firstTime || !NetworkUtils.isOnline(context)) {
            interactor.getOffilinePullRequests(creator, repository)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(pullsResults -> {
                        view.showItems(pullsResults);
                    });
        }

        interactor.getAllPullRequests(creator, repository)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(pullsResults -> {
                            if (firstTime) {
                                view.cleanData();
                                firstTime = false;
                            }

                            view.showItems(pullsResults);
                            view.hideError();
                            view.hideProgress();
                        },
                        error -> {
                            view.hideProgress();
                            view.showError();
                        });
    }
}
