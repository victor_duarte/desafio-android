package com.victorduarte.desafio_android.presenters;

import android.content.Context;

import com.victorduarte.desafio_android.interactors.ApiInteractor;
import com.victorduarte.desafio_android.utils.NetworkUtils;
import com.victorduarte.desafio_android.view.RepositoryListView;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Victor on 25/08/2016.
 */
public class RepositoryListPresenter {
    private int currentTimelinePage = 0;

    private RepositoryListView view;
    private ApiInteractor interactor;

    private boolean firstTime = true;

    public RepositoryListPresenter(RepositoryListView view, ApiInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    public void getRepositories(Context context) {
        view.showProgress();

        if (firstTime || !NetworkUtils.isOnline(context)) {
            interactor.getOfflineRepositories(currentTimelinePage + 1)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .filter(result -> result != null && result.getRepositories() != null)
                    .subscribe(repositoriesSearchResult -> {
                        currentTimelinePage = repositoriesSearchResult.getPagination();
                        view.showItems(repositoriesSearchResult.getRepositories());
                    });
        }

        interactor
                .searchRepositories("language:Java", "stars", 1 + (firstTime ? 0 : currentTimelinePage))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .filter(result -> result != null && result.getRepositories() != null)
                .subscribe(repositoriesSearchResult -> {
                    currentTimelinePage = repositoriesSearchResult.getPagination();

                    if (firstTime) {
                        view.cleanData();
                        firstTime = false;
                    }
                    view.showItems(repositoriesSearchResult.getRepositories());
                    view.hideError();
                    view.hideProgress();
                }, error -> {
                    view.hideProgress();
                    view.showError();
                });
    }
}
