package com.victorduarte.desafio_android.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Victor on 25/08/2016.
 */
public class PullRequest extends RealmObject {

    @SerializedName("id")
    @Expose
    @PrimaryKey
    private int id;

    private String creator;

    private String repository;

    @SerializedName("user")
    @Expose
    private User user;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("body")
    @Expose
    private String body;

    @SerializedName("created_at")
    @Expose
    private Date createdAt;

    @SerializedName("html_url")
    @Expose
    private String htmlUrl;

    public User getUser() {
        return user;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public String getCreator() {
        return creator;
    }

    public String getRepository() {
        return repository;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public void setRepository(String repository) {
        this.repository = repository;
    }
}
