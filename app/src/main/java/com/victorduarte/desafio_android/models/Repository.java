package com.victorduarte.desafio_android.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Victor on 25/08/2016.
 */
public class Repository extends RealmObject {

    @SerializedName("id")
    @Expose
    @PrimaryKey
    private Integer id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("owner")
    @Expose
    private User owner;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("stargazers_count")
    @Expose
    private Integer stargazersCount;

    @SerializedName("forks_count")
    @Expose
    private Integer forksCount;

    @SerializedName("language")
    @Expose
    private String language;

    public User getOwner() {
        return owner;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public int getForksCount() {
        return forksCount;
    }

    public int getStargazersCount() {
        return stargazersCount;
    }

    public String getLanguage() {
        return language;
    }
}
