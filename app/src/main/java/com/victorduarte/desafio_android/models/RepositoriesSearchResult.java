package com.victorduarte.desafio_android.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Victor on 25/08/2016.
 */
public class RepositoriesSearchResult extends RealmObject {
    @PrimaryKey
    int pagination;

    @SerializedName("items")
    @Expose
    private RealmList<Repository> repositories;

    public List<Repository> getRepositories() {
        return repositories;
    }

    public int getPagination() {
        return pagination;
    }

    public void setPagination(int pagination) {
        this.pagination = pagination;
    }
}

