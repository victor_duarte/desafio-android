package com.victorduarte.desafio_android;

import com.victorduarte.desafio_android.models.PullRequest;
import com.victorduarte.desafio_android.models.RepositoriesSearchResult;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Victor on 25/08/2016.
 */
public interface ServiceContract {
    @GET("search/repositories")
    Observable<RepositoriesSearchResult> searchRepository(@Query("q") String filter,
                                                          @Query("sort") String sort,
                                                          @Query("page") int page);

    @GET("repos/{creator}/{repository}/pulls")
    Observable<List<PullRequest>> getAllPullRequests(@Path("creator") String creator,
                                                     @Path("repository") String repository);
}
