package com.victorduarte.desafio_android.utils;

/**
 * Created by Victor on 27/08/2016.
 */
public class IntUtils {
    public static String formatCount(long count) {
        String result;

        if (count == 0) {
            return "0";
        } else if (count > 100000) {
            result = ((count / 100000) + "M");
        } else if (count > 1000) {
            result = ((count / 1000) + "K");
        } else {
            result = Long.toString(count);
        }

        return result.replaceAll("^0+", "");
    }
}
