package com.victorduarte.desafio_android;

import android.support.test.espresso.ViewAssertion;
import android.support.v7.widget.RecyclerView;

import static org.junit.Assert.assertEquals;

/**
 * Created by Victor on 27/08/2016.
 */
public class RecyclerViewAssertions {

    public static ViewAssertion hasItemsCount(final int count) {
        return (view, e) -> {
            if (!(view instanceof RecyclerView)) {
                throw e;
            }

            RecyclerView rv = (RecyclerView) view;
            assertEquals(count, rv.getAdapter().getItemCount());
        };
    }
}