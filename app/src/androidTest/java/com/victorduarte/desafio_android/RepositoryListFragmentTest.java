package com.victorduarte.desafio_android;

import android.content.pm.ActivityInfo;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.victorduarte.desafio_android.interactors.ApiInteractor;
import com.victorduarte.desafio_android.models.Repository;
import com.victorduarte.desafio_android.view.MainActivity;
import com.victorduarte.desafio_android.view.RepositoryListAdapter;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.victorduarte.desafio_android.RecyclerViewAssertions.hasItemsCount;

/**
 * Created by Victor on 27/08/2016.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class RepositoryListFragmentTest {
    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(
            MainActivity.class);

    @Before
    public void setup() {
        activityTestRule.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Test
    public void getItemsCountInRecyclerView_changeOrientation_checkItemsCountInRecyclerView() throws Throwable {
        RecyclerView view = (RecyclerView) activityTestRule.getActivity().findViewById(R.id.repository_list_recyclerview);

        activityTestRule.runOnUiThread(() -> {
            final LinearLayoutManager linearLayoutManager =
                    new LinearLayoutManager(activityTestRule.getActivity());
            linearLayoutManager.setReverseLayout(true);
            view.setLayoutManager(linearLayoutManager);
        });

        InstrumentationRegistry.getInstrumentation().waitForIdleSync();

        int expectedCount = view.getAdapter().getItemCount();

        activityTestRule.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        onView(withId(R.id.repository_list_recyclerview)).check(hasItemsCount(expectedCount));
    }

    @Test
    public void checkListIsCorrectedFiltered() {
        onView(withId(R.id.repository_list_recyclerview)).check(matches(matchesFilter()));
    }

    @Test
    public void selectItem_CheckIsCorrectItem() {
        int position = 3;
        RecyclerView view = (RecyclerView) activityTestRule.getActivity().findViewById(R.id.repository_list_recyclerview);

        String repostitory = ((RepositoryListAdapter) view.getAdapter()).getItem(position).getName();

        onView(withId(R.id.repository_list_recyclerview)).perform(
                RecyclerViewActions.actionOnItemAtPosition(position, click()));

        onView(withText(repostitory)).check(matches(isDisplayed()));
    }

    private static Matcher<View> matchesFilter() {
        return new TypeSafeMatcher<View>() {

            @Override
            protected boolean matchesSafely(View item) {
                RecyclerView recyclerView = (RecyclerView) item;

                RepositoryListAdapter adapter = (RepositoryListAdapter) recyclerView.getAdapter();
                List<Repository> repositories = adapter.getItems();

                int lastStargazerCount = -1;

                for (Repository repository : repositories) {
                    if (!repository.getLanguage().toLowerCase().equals("java")) {
                        return false;
                    }

                    if (lastStargazerCount != -1 && lastStargazerCount < repository.getStargazersCount()) {
                        return false;
                    }

                    lastStargazerCount = repository.getStargazersCount();
                }
                return true;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("");
            }
        };
    }


}
