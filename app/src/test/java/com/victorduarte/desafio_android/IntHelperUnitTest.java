package com.victorduarte.desafio_android;

import com.victorduarte.desafio_android.utils.IntUtils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class IntHelperUnitTest {

    @Test
    public void formatCount_lessThan1000_isCorrect() throws Exception {
        assertEquals(IntUtils.formatCount(123), "123");
    }

    @Test
    public void formatCount_moreThan1000_isCorrect() throws Exception {
        assertEquals(IntUtils.formatCount(1234), "1K");
    }

    @Test
    public void formatCount_moreThan100000_isCorrect() throws Exception {
        assertEquals(IntUtils.formatCount(123456), "1M");
    }
}